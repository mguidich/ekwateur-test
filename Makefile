EXEC=docker-compose exec php /bin/bash
all: up dependency cache

up:
	docker-compose up -d

dependency:
	$(EXEC) -c 'composer install --no-suggest --no-progress'
