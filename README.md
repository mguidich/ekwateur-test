# Ekwateur Test

## Requirements

Docker
php 8.0

## Getting started

Clone Project
git clone git@gitlab.com:mguidich/ekwateur-test.git

##  Run project

make all

## Execute Command

docker-compose exec php /bin/bash -c 'php bin/console  promo-code:validate CODE_TO_TEST'
