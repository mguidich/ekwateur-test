<?php

namespace App\Serialize;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class DefaultSerializeFactory.
 */
class DefaultSerializeFactory
{
    public function createSerializer(): Serializer
    {
        $defaultContext = [
            AbstractNormalizer::CALLBACKS => [
                'endDate' => function ($innerObject) {
                    return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d') : '';
                },
            ],
        ];

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        $serializer = new Serializer(
            [new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter, null, null, null, null, $defaultContext)],
            ['json' => new JsonEncode()]
        );

        return $serializer;
    }
}
