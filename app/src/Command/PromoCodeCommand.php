<?php

namespace App\Command;

use App\Serialize\DefaultSerializeFactory;
use App\Validation\PromoCodeValidationHandler;
use DateTime;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\SerializerInterface;

class PromoCodeCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';

    private PromoCodeValidationHandler $promoCodeValidationHandler;
    private SerializerInterface $serializer;
    private ParameterBagInterface $parameterBag;
    private mixed $path;

    /**
     * PromoCodeCommand constructor.
     */
    public function __construct(
        PromoCodeValidationHandler $promoCodeValidationHandler,
        DefaultSerializeFactory $serializerFactory,
        ParameterBagInterface $parameterBag,
    ) {
        $this->promoCodeValidationHandler = $promoCodeValidationHandler;
        $this->serializer = $serializerFactory->createSerializer();
        $this->parameterBag = $parameterBag;
        $this->path = $this->parameterBag->get('JSON_OUTPUT');
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('code', InputArgument::REQUIRED, 'The code to validate')
            ->setDescription('Code validate in offers')
            ->setHelp('This command allows to retrun the list of offer from code given , output is a file json in folder');
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $code = $input->getArgument('code');
        try {
            $promotionCode = $this->promoCodeValidationHandler->handle($code);
            $jsonOutput = $this->serializer->serialize($promotionCode, 'json', ['groups' => 'output']);

            $filesystem = new Filesystem();
            if (!$filesystem->exists($this->path)) {
                $filesystem->mkdir($this->path);
            }

            $date = new DateTime();
            $fileName = 'code_'.$code.'_'.$date->format('Y-m-d_H-s').'.json';
            $filePath = $this->path.$fileName;
            $filesystem->dumpFile($filePath, $jsonOutput);

            $output->writeln("<info>File Json is created in public directory with Code '{$code}' </info>");
            $output->writeln("File content :<fg=#1E90FF> {$jsonOutput}</>");
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }

        return Command::SUCCESS;
    }
}
