<?php

namespace App\Model;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Class Offers.
 */
class Offers
{
    protected string $type;
    protected string $name;
    protected string $description;
    protected array $validPromoCodeList;

    /**
     * @param $data
     */
    public static function createFromArray($data): Offers
    {
        $new = new Offers();
        $new->setType($data['offerType']);
        $new->setName($data['offerName']);
        $new->setDescription($data['offerDescription']);
        $new->setValidPromoCodeList($data['validPromoCodeList']);

        return $new;
    }

    /**
     * @SerializedName("type")
     * @Groups({"output"})
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return $this
     */
    public function setType(string $type): Offers
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @SerializedName("name")
     * @Groups({"output"})
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): Offers
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return $this
     */
    public function setDescription(string $description): Offers
    {
        $this->description = $description;

        return $this;
    }

    public function getValidPromoCodeList(): array
    {
        return $this->validPromoCodeList;
    }

    /**
     * @return $this
     */
    public function setValidPromoCodeList(array $validPromoCodeList): Offers
    {
        $this->validPromoCodeList = $validPromoCodeList;

        return $this;
    }
}
