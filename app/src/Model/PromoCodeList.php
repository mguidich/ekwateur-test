<?php

namespace App\Model;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Class PromoCodeList.
 */
class PromoCodeList
{
    /**
     * @Groups({"output"})
     */
    protected string $code;

    /**
     * @Groups({"output"})
     */
    protected float $discountValue;

    /**
     * @Groups({"output"})
     */
    protected DateTime $endDate;

    private ArrayCollection $compatibleOffersList;

    /**
     * PromoCodeList constructor.
     */
    public function __construct()
    {
        $this->compatibleOffersList = new ArrayCollection();
    }

    /**
     * @param $data
     *
     * @throws \Exception
     */
    public static function createFromArray($data): PromoCodeList
    {
        $new = new PromoCodeList();
        $new->setCode($data['code']);
        $new->setDiscountValue($data['discountValue']);
        $new->setEndDate(new DateTime($data['endDate']));

        return $new;
    }

    /**
     * @SerializedName("code")
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return $this
     */
    public function setCode(string $code): PromoCodeList
    {
        $this->code = $code;

        return $this;
    }

    public function getDiscountValue(): float
    {
        return $this->discountValue;
    }

    /**
     * @return $this
     */
    public function setDiscountValue(float $discountValue): PromoCodeList
    {
        $this->discountValue = $discountValue;

        return $this;
    }

    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @return $this
     */
    public function setEndDate(DateTime $endDate): PromoCodeList
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @Groups({"output"})
     */
    public function getcompatibleOffersList(): ArrayCollection
    {
        return $this->compatibleOffersList;
    }

    /**
     * @return $this
     */
    public function setcompatibleOffersList(ArrayCollection $compatibleOffersList): PromoCodeList
    {
        $this->compatibleOffersList = $compatibleOffersList;

        return $this;
    }

    public function addCompatibleOffer(Offers $offers): PromoCodeList
    {
        $this->compatibleOffersList[] = $offers;

        return $this;
    }
}
