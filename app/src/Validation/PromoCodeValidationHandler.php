<?php

namespace App\Validation;

use App\Services\DataOffersInterface;
use App\Services\DataPromoCodeListInterface;
use Exception;

/**
 * Class PromoCodeValidationHandler.
 */
class PromoCodeValidationHandler
{
    private DataOffersInterface $dataOffers;
    private DataPromoCodeListInterface $dataPromoCodeList;

    /**
     * PromoCodeValidationHandler constructor.
     */
    public function __construct(DataOffersInterface $dataOffers, DataPromoCodeListInterface $dataPromoCodeList)
    {
        $this->dataOffers = $dataOffers;
        $this->dataPromoCodeList = $dataPromoCodeList;
    }

    /**
     * @param null $code
     *
     * @return false|mixed
     *
     * @throws Exception
     */
    public function handle($code = null)
    {
        $promoCodes = $this->dataPromoCodeList->fetch($code);

        if (empty($promoCodes)) {
            throw new \Exception(sprintf('Not promo found for this code "%s" ', $code));
        }

        $offers = $this->dataOffers->fetch();
        $promoCode = reset($promoCodes);
        $compatibleOffersList = array_filter($offers, function ($offer) use ($promoCode) {
            return in_array($promoCode->getCode(), $offer->getValidPromoCodeList());
        });

        if (empty($compatibleOffersList)) {
            throw new \Exception(sprintf('Not offer found with code "%s" ', $code));
        }

        foreach ($compatibleOffersList as $offer) {
            $promoCode->addCompatibleOffer($offer);
        }

        return $promoCode;
    }
}
