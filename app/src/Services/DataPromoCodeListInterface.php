<?php

namespace App\Services;

interface DataPromoCodeListInterface
{
    /**
     * @param null $code
     */
    public function fetch($code = null): array;
}
