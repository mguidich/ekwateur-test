<?php

namespace App\Services;

interface DataOffersInterface
{
    public function fetch(): array;
}
