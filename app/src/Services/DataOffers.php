<?php

namespace App\Services;

use App\Api\ContentProviderInterface;
use App\Model\Offers;

/**
 * Class DataOffers.
 */
class DataOffers implements DataOffersInterface
{
    private ContentProviderInterface $contentProvider;
    public const URI_OFFERS = '/offerList';

    /**
     * DataOffers constructor.
     */
    public function __construct(ContentProviderInterface $contentProvider)
    {
        $this->contentProvider = $contentProvider;
    }

    public function fetch(): array
    {
        $content = $this->contentProvider->getContent(self::URI_OFFERS);
        $offers = [];

        foreach ($content as $row) {
            $offer = Offers::createFromArray($row);
            $offers[] = $offer;
        }

        return $offers;
    }
}
