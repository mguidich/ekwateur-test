<?php

namespace App\Services;

use App\Api\ContentProviderInterface;
use App\Model\PromoCodeList;

/**
 * Class DataPromoCodeList.
 */
class DataPromoCodeList implements DataPromoCodeListInterface
{
    private ContentProviderInterface $contentProvider;
    public const URI_PROMO_CODE_LIST = '/promoCodeList';

    /**
     * FetchPromotions constructor.
     */
    public function __construct(ContentProviderInterface $contentProvider)
    {
        $this->contentProvider = $contentProvider;
    }

    /**
     * @param null $code
     *
     * @throws \Exception
     */
    public function fetch($code = null): array
    {
        $content = $this->contentProvider->getContent(self::URI_PROMO_CODE_LIST);

        $promoCodeLists = [];

        foreach ($content as $row) {
            $promoCodeList = PromoCodeList::createFromArray($row);
            if (null !== $code && $promoCodeList->getCode() != $code) {
                continue;
            }

            $promoCodeLists[] = PromoCodeList::createFromArray($row);
        }

        return $promoCodeLists;
    }
}
