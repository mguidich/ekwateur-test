<?php

namespace App\Api;

interface ContentProviderInterface
{
    /**
     * @param $uri
     */
    public function getContent($uri): array;
}
