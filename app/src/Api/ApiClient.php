<?php

namespace App\Api;

use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiClient implements ContentProviderInterface
{
    private HttpClientInterface $httpClient;
    private ParameterBagInterface $parameterBag;
    private mixed $baseUrl;

    /**
     * ApiClient constructor.
     */
    public function __construct(HttpClientInterface $httpClient, ParameterBagInterface $parameterBag)
    {
        $this->httpClient = $httpClient;
        $this->parameterBag = $parameterBag;
        $this->baseUrl = $this->parameterBag->get('API_URL');
    }

    /**
     * @param $uri
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getContent($uri): array
    {
        $apiUrl = rtrim($this->baseUrl, '/');
        try {
            $response = $this->httpClient->request(
                'GET',
                $apiUrl.$uri
            );

            return $response->toArray();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
